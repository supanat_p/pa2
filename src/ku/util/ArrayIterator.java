/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package ku.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An ArrayIterator is  that can keep any object.
 * It can pass index that null 
 * @author Supanat Pokturng 
 * @param <T> is any Object to input in this array
 * @version 2015.02.19
 */
public class ArrayIterator<T> implements Iterator<T> {
	
	/** An array that can keep any object inside. */
	private T[] array;
	
	/** Use to tell us where is current index. */
	private int cursor;
	
	/**
	 * Constructor of this class.
	 * Initial cursor to zero
	 * @param array is array that keep any object
	 */
	public ArrayIterator( T[] array) {
		this.array = array;
		cursor = 0;
	}
	
	/**
	 * Check that after this cursor index
	 * have any index that's not null or not.
	 * @return true if after cursor have some index 
	 * that not null
	 */
	public boolean hasNext() {
		for(int i=this.cursor  ; i <array.length ; i++) {
			if(this.array[i] != null)
				return true;
		}
		return false;
	}
	
	/**
	 * Get an object that next to the cursor's index.
	 * And move cursor to that object's index if there have
	 * @return object that next to the cursor's index if there have
	 */
	public T next() {
		if(hasNext()) {
			for(int i=cursor ; i<this.array.length ; i++) {
				this.cursor++;
				if(this.array[i] != null){
					return this.array[i];
				}
			}
		}
		throw new NoSuchElementException( );
	}
	
	/**
	 * Remove an object before the cursor's index.
	 */
	public void remove() {
		this.array[cursor - 1] = null;
	}
}
