/**
 * This source code is Copyright 2015 by Supanat Pokturng
 */
package ku.util;

import java.util.EmptyStackException;

/**
 * Stack class that keep object inside.
 * can fix capacity of this box
 * can get the outer object
 * @author Supanat Pokturng
 * @param <T> is any object that get inside array
 * @version 2015.02.19
 */
public class Stack<T> {
	
	/** Array that keep object inside. */
	private T[] items;
	
	/** Quantity of objects inside the array. */
	private int numberItems = 0;
	
	/**
	 * Constructor of Stack class that fix the max
	 * number of objects.
	 * @param capacity is max number of objects inside the array
	 */
	public Stack( int capacity ) {
		if(capacity>0) {
			this.items = (T[]) new Object[capacity];
		}
	}
	
	/**
	 * Get max number of the array.
	 * @return max number of the array.
	 */
	public int capacity() {
		return items.length;
	}
	
	/**
	 * Check that this array is empty or not.
	 * @return true if number of item is zero
	 */
	public boolean isEmpty() {
		return numberItems == 0;
	}
	
	/**
	 * Check that this array is full of objects or not.
	 * @return true if number of items in array 
	 * is equals maximum number
	 */
	public boolean isFull() {
		return numberItems == items.length;
	}
	
	/**
	 * If this array not empty then get an object
	 * that is on the top.
	 * @return an object that is outer of the array
	 */
	public T peek() {
		if( isEmpty() )
			return null;
		return items[this.size() - 1];
	}
	
	/**
	 * If this array is not empty, get an object that
	 * on the top and remove its in array.
	 * @return an object that is on the top
	 */
	public T pop() {
		if( !isEmpty() ) {
			T obj = peek();
			this.items[this.size() - 1] = null;
			this.numberItems--;
			return obj;
		}
		else
			throw new EmptyStackException();
			
	}
	
	/**
	 * If the array is not full, it will get this object
	 * into the array.
	 * @param obj is obj that want to keep in the array
	 */
	public void push( T obj ) {
		if( !isFull() ) {
			if( obj == null ) {
				throw new IllegalArgumentException();
			}
			this.items[size()] = obj;
			this.numberItems++;
		}
	}
	
	/**
	 * Get number of items in the array.
	 * @return number of items in the array
	 */
	public int size() {
		return numberItems;
	}
}
